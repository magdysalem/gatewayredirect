﻿using OzNet.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;


namespace GatewayRedirect
{
    public class Router
    {

        public List<Route> Routes { get; set; }
        public string Destination { get; set; }
        public Router(string routeConfigFilePath)
        {
            dynamic router = JsonLoader.LoadFromFile<dynamic>(routeConfigFilePath);

            Routes = JsonLoader.Deserialize<List<Route>>(Convert.ToString(router.routes));
            Console.WriteLine(Routes);
        }

        public async Task<HttpResponseMessage> RouteRequest(HttpRequest request)
        {
            string path = request.Path.ToString();
            //string basePath = '/' + path.Split('/')[1];

             Console.WriteLine(path);
             Console.WriteLine(path);

            
            try
            {

                Console.WriteLine("Get Dest based on "+ path);

                Destination = Routes.First(r => r.Endpoint.Equals(path)).Destination;
                Console.WriteLine(Destination);
                request.HttpContext.Response.Redirect(Destination);
                return OK();

            }
            catch
            {
                return ConstructErrorMessage("The path could not be found.");
            }
            
        }

        private HttpResponseMessage OK()
        {
            HttpResponseMessage okMsg = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent("200")
            };
            return okMsg;
        }

        private HttpResponseMessage ConstructErrorMessage(string error)
        {
            HttpResponseMessage errorMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(error)
            };
            return errorMessage;
        }

    }
}
