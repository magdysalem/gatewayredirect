﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GatewayRedirect
{
    public class Route
    {
        public string Endpoint { get; set; }
        public string Destination { get; set; }
    }
}
